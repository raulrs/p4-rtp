## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?:  1268 paquetes
* ¿Cuánto tiempo dura la captura?: 12810 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 pps
* ¿Qué protocolos de nivel de red aparecen en la captura?: El principal protocolo de nivel de red que aparece es IPv4.
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: Protocolos UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: En este caso nuestro protocolo de nivel de aplicación es RTP ya que se utiliza para transmisión de datos multimedia.
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 14
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete:54550
* SSRC del paquete:0x2A173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU(0). Se trata de datos de audio ya que encontramos un códec muy utilizado en llamadas VoIP. Lo hallamos debido al valor "0" de PCMU(nº de identificación del tipo de datos).
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 4
* ¿Cuántos paquetes hay en el flujo F?: 626
* ¿El origen del flujo F es la máquina A o B?: B, porque el primer paquete de todos se envía de 192.168.0.10(A) a 216.234.64.16(B) y vemos que la source es 216...
* ¿Cuál es el puerto UDP de destino del flujo F?: 49154
* ¿Cuántos segundos dura el flujo F?: 12.49
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 21.187000 ms
* ¿Cuál es el jitter medio del flujo?: 0.229065
* ¿Cuántos paquetes del flujo se han perdido?: 0(0.0%)
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 0.431875ms que proviene de realizar la diferencia entre el max jitter y el tiempo que nos dice como referencia de 0.4ms.
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 14
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Ha llegado pronto ya que los valores de skew del paquete 14 tiene un valor negativo y por tanto indica que ha llegado antes. 
* ¿Qué jitter se ha calculado para ese paquete? Lo podemos ver desde telefonía, RTP Stream Analysis y ver que el jitter es de 4.370271ms
* ¿Qué timestamp tiene ese paquete? 1120
* ¿Por qué número hexadecimal empieza sus datos de audio? 00
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: lun 23 oct 2023 12:58:36 CEST
* Número total de paquetes en la captura: 11286
* Duración total de la captura (en segundos): 49.508 s
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: Hay 1408, contando los paquetes ICMP de error también(450).
* ¿Cuál es el SSRC del flujo?: 0xb1770cf2
* ¿En qué momento (en ms) de la traza comienza el flujo? Aproximadamente a los 39s.
* ¿Qué número de secuencia tiene el primer paquete del flujo? 25739
* ¿Cuál es el jitter medio del flujo?: 0ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
* Se valorará que el fichero de respuestas esté en formato correcto (si no es así, es posible que no se corrija).
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.
